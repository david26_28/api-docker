from django.contrib import admin
from database.models import Rooms, Buildings, Storeys
# Register your models here.

class RoomAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'storey_id']
    ordering = ['name']
    actions = []

class BuildingAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'address']
    ordering = ['name']
    actions = []

class StoreyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'building_id']
    ordering = ['name']
    actions = []

admin.site.register(Rooms, RoomAdmin)
admin.site.register(Buildings, BuildingAdmin)
admin.site.register(Storeys, StoreyAdmin)