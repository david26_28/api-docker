from rest_framework import serializers
from database.models import Buildings, Rooms, Storeys
#Übersetzten der Models ins JSON-Format

class BuildingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Buildings
        fields = ('id', 'name', 'address')

class StoreySerializer(serializers.ModelSerializer):
    class Meta:
        model = Storeys
        fields = ('id', 'name', 'building')

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rooms
        fields = '__all__'