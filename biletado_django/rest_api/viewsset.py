from rest_framework import viewsets
from database import models
from . import serializers
import logging

logger = logging.getLogger(__name__)

class BuildingsViewset(viewsets.ModelViewSet):
    queryset = models.Buildings.objects.all()
    serializer_class = serializers.BuildingSerializer
    logger.debug("Use BuildingsViewset")

class StoreysViewset(viewsets.ModelViewSet):
    queryset = models.Storeys.objects.all()
    serializer_class = serializers.StoreySerializer
    logger.debug("Use StoreysViewset")

class RoomViewset(viewsets.ModelViewSet):
    queryset = models.Rooms.objects.all()
    serializer_class = serializers.RoomSerializer
    logger.debug("Use RoomViewset")