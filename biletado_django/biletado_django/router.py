from rest_api.viewsset import BuildingsViewset, StoreysViewset, RoomViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('buildings', BuildingsViewset)
router.register('storeys', StoreysViewset)
router.register('rooms', RoomViewset)