This Container is a API for Biletado.

used with https://gitlab.com/david26_28/compose it is available as localhost/api/assets or localhost/django

Our Framework of choise is Django because we know it a litte and know python.



NOTES:
build with
'''
docker build --tag python-django .
'''
run with
'''
docker run --publish 8000:8000 python-django
'''

server should be available under 127.0.0.1:8000

## Simple Container:
Unser Container basiert auf Python. Zusätzlich wurden pip sowie einige Django Bibliotheken `src/requirements.txt` installiert. Anschließend wurden die lokalen Dateien in dan Arbeitsverzeichnis ds Containers kopiert. Ebenso wird der Container variable auf Basis von Umgebungsvariablen aus der `.env` Datei erstellt.

## Framework:
Als Framework haben wir uns für Django entschieden, da wir beide bereits mit Python georbeitet hatten und die anderen Frameworks auch noch nie verwendet hatten.

## SQL-Injection:
Unser Backend ist vor SQL-Injektions geschützt. Die Eingabeparameter werden gegen das Datenbankmodell validiert bevor sie an PostgreSQL übertragen werden. Dies wurde auch durch einige Tests bestätigt.

## validating JWA
Django erstellt selbst JWA token, diese sind durch eingabe von Username unds Passwort unter `/api/token` abrufbar. Eine Testklasse die eine Autentifizierung benötigt ist unter `/api/token/hello` abrufbar.

## Tests
Unsere GitLap-CI erstellt bei jedem Commit ein neues Image des Containers. Dabei wird auch getestet, ob der Container funktioniert. Falls nicht wird kein Image veröffentlicht.
Eine weitere Testmöglichkeit stellt das Postman-File dar. Dieses kann einfach in die Postmann Applikation importiert werden und dient beispielhabt dazu alle möglichen CRUD-Operatoren zu testen. Für die Bereiche `buildings`und `storeys` kann der Workspace dubliziert werden und die Worcspace spezifische Enviorment-Variable `roomURL`ersetzt werden. Zudem kann durch Postman die Autentifizierung über JWA-Token getestet werden.



