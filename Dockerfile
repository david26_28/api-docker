FROM python:3.10.4 as Django-Container



WORKDIR /usr/src/app

COPY ./biletado_django /usr/src/app
COPY ./src /usr/src/app
#VOLUME ./src

#Installlationen
RUN apt-get update -y && apt-get upgrade -y
RUN python -m pip install -r requirements.txt
RUN apt-get install apache2 postgresql -y

#RUN django-admin startproject biletado_django
#RUN python manage.py makemigrations
#RUN python manage.py migrate
CMD ["python","manage.py","runserver","0.0.0.0:8000"]

EXPOSE 8000:8000
